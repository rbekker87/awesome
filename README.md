# My Awesome List [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome)

A curated list of awesome software and resources.

Inspired by [awesome-python](https://github.com/vinta/awesome-python).

## Table of Contents
- [Awesome Python](#awesome-python)
- [API Clients](#api-clients)
- [Bootstrap Templates](#bootstrap-templates)
- [Continuous Integration](#continuous-integration)
- [DevOps Tools](#devops-tools)
- [Environment Management](#environment-management)
- [Image Hosting](#image-hosting)
- [Package Management](#package-management)
- [Self Hosted](#self-hosted)
- [Static File Hosting](#static-file-hosting)
- [Static Site Generators](#static-site-generators)
- [URL Shorteners](#url-shorteners)
- [Other Awesome Lists](#other-awesome-lists)

- - -
## DevOps Tools

*Software and libraries for DevOps.*

* [Ansible](https://github.com/ansible/ansible) - A radically simple IT automation platform.
* [SaltStack](https://github.com/saltstack/salt) - Infrastructure automation and management system.
* [OpenStack](http://www.openstack.org/) - Open Source software for building private and public clouds.
* [Docker Compose](https://docs.docker.com/compose/) - Fast, isolated development environments using [Docker](https://www.docker.com/).
* [Cloud-Init](http://cloudinit.readthedocs.io/) - A multi-distribution package that handles early initialization of a cloud instance.
* [cuisine](https://github.com/sebastien/cuisine) - Chef-like functionality for Fabric.
* [Fabric](http://www.fabfile.org/) - A simple, Pythonic tool for remote execution and deployment.
* [Fabtools](https://github.com/ronnix/fabtools) - Tools for writing awesome Fabric files.
* [honcho](https://github.com/nickstenning/honcho) - A Python clone of [Foreman](https://github.com/ddollar/foreman), for managing Procfile-based applications.
* [pexpect](https://github.com/pexpect/pexpect) - Controlling interactive programs in a pseudo-terminal like GNU expect.
* [psutil](https://github.com/giampaolo/psutil) - A cross-platform process and system utilities module.
* [supervisor](https://github.com/Supervisor/supervisor) - Supervisor process control system for UNIX.

## API Clients

*API Clients for Services*

* [libvirt](https://libvirt.org/python.html) - Python Client for Libvirt
* [pylxd](https://pylxd.readthedocs.io/en/latest/) - Python Client for LXD

## Bootstrap Templates

* [marketing-justified-nav](https://getbootstrap.com/examples/justified-nav/)

## Continuous Integration

*See: [awesome-CIandCD](https://github.com/ciandcd/awesome-ciandcd#online-build-system).*

* [Travis CI](https://travis-ci.org) - A popular CI service for your open source and [private](https://travis-ci.com) projects. (GitHub only)

## Environment Management

*Libraries for Python version and environment management.*

* [p](https://github.com/qw3rtman/p) - Dead simple interactive Python version management.
* [pyenv](https://github.com/yyuu/pyenv) - Simple Python version management.

## Image Hosting

*Free Image Hosting Services*

* [Post Image](https://postimage.org/) - Post Image Hosting Service.


## Package Management

*Libraries for package and dependency management.*

* [pip](https://pip.pypa.io/en/stable/) - The Python package and dependency manager.
    * [Python Package Index](https://pypi.python.org/pypi)
* [pip-tools](https://github.com/nvie/pip-tools) - A set of tools to keep your pinned Python dependencies fresh.

## Self Hosted

*List of awesome self hosted software*

* [Dokku](https://github.com/dokku-alt/dokku-alt) - Docker powered Mini-Heroku

## Static File Hosting

*List of static hosting providers*

* [Surge](http://surge.sh/) - Surge CLI
* [Pancakes](https://www.pancake.io) - Dropbox, Git Integration
* [Firebase](https://firebase.google.com) - Google, cli

## Static Site Generators

* [harpjs](https://harpjs.com)
* [octopress](http://octopress.org)

## URL Shorteners

*List of URL Shorteners*

* [BitLy](https://bitly.com/)
* [Google](https://goo.gl)

## Other Awesome Lists

List of lists.

* AWS
    * [donnemartin](https://github.com/donnemartin/awesome-aws)

* Docker
    * [veeggiemonk](https://github.com/veggiemonk/awesome-docker)

* Flask
    * [humiaozuzu](https://github.com/humiaozuzu/awesome-flask)

* Kubernetes
    * [ramitsurana](https://github.com/ramitsurana/awesome-kubernetes)

* Micro Services
    * [microservices](https://github.com/mfornos/awesome-microservices)

* Python
    * [vinta](https://github.com/vinta/awesome-python)
    * [uhub](https://github.com/uhub/awesome-python)
    * [python_reference](https://github.com/rasbt/python_reference)
    * [kirang89](https://github.com/kirang89/pycrumbs)

* Self Hosted
   * [kickball](https://github.com/Kickball/awesome-selfhosted)

* Sysadmin
   * [kahun](https://github.com/kahun/awesome-sysadmin)

* Linux Software
   * [linux](https://github.com/VoLuong/Awesome-Linux-Software)

    
